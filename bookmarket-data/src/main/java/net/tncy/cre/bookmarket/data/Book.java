package net.tncy.cre.bookmarket.data;

import net.tncy.cre.validator.ISBN;

public class Book {
    public enum BookFormat {
        BROCHE,
        POCHE
    }
    public int id;
    public String title;
    public String author;
    public String publisher;
    public BookFormat format;

    @ISBN
    public String isbn;

    public Book(int id, String title, String author, String publisher, BookFormat format, String isbn) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.publisher = publisher;
        this.format = format;
        this.isbn = isbn;
    }
}
