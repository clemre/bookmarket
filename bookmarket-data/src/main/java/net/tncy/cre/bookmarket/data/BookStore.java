package net.tncy.cre.bookmarket.data;

public class BookStore {
    public int id;
    public String name;
    public InventoryEntry[] inventoryEntries;

    public BookStore(int id, String name, InventoryEntry[] inventoryEntries) {
        this.id = id;
        this.name = name;
        this.inventoryEntries = inventoryEntries;
    }
}
